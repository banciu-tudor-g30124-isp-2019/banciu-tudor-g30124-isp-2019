package ex3;

public class BankTest {
    public static void main(String[] args) {
        Bank l = new Bank();
        l.addAccount("Banciu Tudor", 250);
        l.addAccount("B Tudor", 310);
        l.addAccount("Mihai", 444110);
        l.addAccount("Daniel Andrei Mocanu", 33312000);
        l.addAccount("Zaraza", 432100);

        //l.printAccounts();
        //l.printAccounts(100, 1e+6);
        l.getAllAccounts();
    }
}
