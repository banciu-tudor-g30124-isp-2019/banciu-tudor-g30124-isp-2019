package ex2;

import java.util.Objects;

public class BankAccount {
    public String owner;
    public double balance;

    public BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public BankAccount(String owner){
        this.owner=owner;
    }

    public String getOwner(){
        return this.owner;
    }

    public double getBalance(){
        return this.balance;
    }

    public void withdraw(double ammount){
        if(this.balance==0){
            System.out.println("Nu mai aveti bani in cont!");
        }
        else if(ammount>this.balance){
            System.out.println("Suma dorita pentru extragere este mai mare decat soldul curent al contului!");
        }
        else{
            this.balance-=ammount;
        }
    }

    public void deposit(double ammount){
        this.balance+=ammount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return owner.equals(that.owner);
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.owner);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "name='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
