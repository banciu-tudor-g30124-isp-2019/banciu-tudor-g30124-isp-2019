package ex2;

import java.util.ArrayList;
import java.util.Comparator;

public class Bank {
    private ArrayList<BankAccount> l = new ArrayList<> ();

    void printAccounts() {
        BalanceCompare balanceCompare = new BalanceCompare ();
        l.sort (balanceCompare);
        for (BankAccount o : l) {
            System.out.println (o);
        }
    }

    public void addAccount(String owner, double balance) {
        l.add (new BankAccount (owner, balance));
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount o : l) {
            if ((o.getBalance () > minBalance) && (o.getBalance () < maxBalance)) {
                System.out.println (o);
            }
        }
    }

    public void getAllAccounts() {
        OwnerCompare ownerCompare = new OwnerCompare ();
        l.sort (ownerCompare);
        for (BankAccount o : l) {
            System.out.println (o);
        }
    }
}

class OwnerCompare implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.getOwner ().compareTo (o2.getOwner ());

    }
}

class BalanceCompare implements Comparator<BankAccount> {
    public int compare(BankAccount o1, BankAccount o2) {
        return Double.compare (o1.getBalance (), o2.getBalance ());
    }
}
