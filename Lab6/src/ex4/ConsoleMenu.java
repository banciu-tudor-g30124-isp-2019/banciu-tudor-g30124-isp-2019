package ex4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary();
        dictionary.addWord("CUVANT", "Unitate de bază a vocabularului, care reprezintă asocierea unui sens (sau a unui complex de sensuri) și a unui complex sonor");
        dictionary.addWord("AC", "Mică ustensilă de oțel, subțire, ascuțită și lustruită, prevăzută cu un orificiu prin care se trece un fir care servește la cusut.");
        dictionary.addWord("DAC", "Persoană care făcea parte din populația de bază a Daciei.");
        dictionary.addWord("BENEFICIAR","Persoană, colectivitate sau instituție care are folos din ceva; destinatar al unor bunuri materiale sau al unor servicii.");
        /*dictionary.addWord();
        dictionary.addWord();
        dictionary.addWord();
        dictionary.addWord();*/

        System.out.println("Alegeti actiunea pe care doriti sa o faceti: ");
        System.out.println("1. Afisarea tuturor cuvintelor");
        System.out.println("2. Afisarea tuturor definitiilor");
        System.out.println("3. Afisarea definitiei unui cuvant dat");

        Scanner keyboard = new Scanner(System.in);
        int option = keyboard.nextInt();

        switch (option) {
            case 1:
                dictionary.getAllWords();
                break;
            case 2:
                dictionary.getAllDefinitions();
                break;
            case 3:
                System.out.println("Introduceti cuvantul dorit: ");
                String option2 = keyboard.next();
                System.out.println(dictionary.getDefinition(option2));
                break;
        }
    }
}
