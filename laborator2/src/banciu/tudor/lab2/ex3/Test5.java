/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.lab2.ex3;
import java.util.Scanner;

/**
 *
 * @author Sala 310
 */
public class Test5 {
    static int prim(int x, int div){
        if(x==1) return 0;
        if(div*div>x) return 1;
        if(x%div==0) return 0;
        return prim(x,div+1);
    }
    public static void main(String[] args){
        Scanner keyboard= new Scanner(System.in);
        System.out.println("Introduceti numerele:\na= ");
        int a=keyboard.nextInt();
        System.out.println("b= ");
        int b=keyboard.nextInt();
        for(int i=a;i<=b;i++){
            if(prim(i,2)==1){
                System.out.println(i);
            }
        }
    }
}
