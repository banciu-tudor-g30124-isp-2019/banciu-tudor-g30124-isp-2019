package banciu.tudor.lab8.ex4;

public class HotEvent extends Event {
    private boolean hot;

    HotEvent(boolean hot) {
        super(EventType.HOT);
        this.hot = hot;
    }

    boolean isHot() {
        return hot;
    }

    @Override
    public String toString() {
        return "HotEvent{" + "hot= " + hot + '}';
    }
}
