package banciu.tudor.lab8.ex4;

public class ColdEvent extends Event {
    private boolean cold;

    ColdEvent(boolean cold) {
        super(EventType.COLD);
        this.cold = cold;
    }

    boolean isCold() {
        return cold;
    }

    @Override
    public String toString() {
        return "ColdEvent{cold=" + cold + "}";
    }

}
