package banciu.tudor.lab8.ex4;

public class TemperatureEvent extends Event {
    private int value;

    TemperatureEvent(int value) {
        super(EventType.FIRE.TEMPERATURE);
        this.value = value;
    }

    int getvalue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("TemperatureEvent{value=%d}", value);
    }
}
