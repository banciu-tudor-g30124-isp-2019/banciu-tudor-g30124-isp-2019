package banciu.tudor.g30124.lab5.ex2;

public class ImageTest {
    public static void main(String args[]) {
        Image img, img2, img3;
        img = new ProxyImage("OK");
        img2= new RotatedImage("OK ");
        img3=new RealImage("ok");
        img.display();
        img2.display();
        img3.display();
    }
}
