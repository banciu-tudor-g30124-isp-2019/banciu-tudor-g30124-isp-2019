package banciu.tudor.g30124.lab5.ex3;

public class Controller {
    Sensor temp;
    Sensor light;

    public void control(int a, int b) {
        for (int i = 0; i < b; i += a) {
            this.temp = new TemperatureSensor();
            this.light = new LightSensor();
            System.out.println("Temperature: " + Integer.toString(temp.readValue()) + " Light: " + Integer.toString(light.readValue()));
        }
    }
}
