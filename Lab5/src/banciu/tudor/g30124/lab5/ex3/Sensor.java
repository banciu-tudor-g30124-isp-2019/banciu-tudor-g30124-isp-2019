package banciu.tudor.g30124.lab5.ex3;

public abstract class Sensor {
    private String location;

    public void Sensor(String S) {
        this.location = S;
    }

    public abstract int readValue();

    public String getLocation() {
        return this.location;
    }
}
