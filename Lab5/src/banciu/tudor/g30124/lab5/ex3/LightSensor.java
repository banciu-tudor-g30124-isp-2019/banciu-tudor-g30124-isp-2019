package banciu.tudor.g30124.lab5.ex3;
import java.util.Random;

public class LightSensor extends Sensor{
    int light;

    public void setLight(int light) {
        this.light = light;
    }

    LightSensor() {
        Random Lum = new Random();
        this.light = Lum.nextInt(100);
    }

    @Override
    public int readValue() {
        return this.light;
    }
}
