package banciu.tudor.g30124.lab5.ex3;
import java.util.Random;

public class TemperatureSensor extends Sensor {
    int temp;

    public void setTemperature(int temp) {
        this.temp = temp;
    }
    
    TemperatureSensor() {
        Random tmp = new Random();
        this.temp = tmp.nextInt(100);
    }

    @Override
    public int readValue() {
        return this.temp;
    }
}
