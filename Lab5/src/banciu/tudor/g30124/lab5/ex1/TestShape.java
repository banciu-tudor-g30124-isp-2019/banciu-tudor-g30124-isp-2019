package banciu.tudor.g30124.lab5.ex1;

public class TestShape {
    public static void main(String[] args){
        Circle cerc = new Circle();
        Rectangle dreptunghi = new Rectangle();
        Square patrat = new Square();
        
        patrat.setFilled(false);
        cerc.setColor("blue");
        dreptunghi.setLength(2);
        dreptunghi.setWidth(2.5);
        patrat.setLength(2);
        patrat.setWidth(2.5);
        
        System.out.println("Aria patratului este: " + patrat.getArea());
        System.out.println("Este patratul umplut: " + patrat.isFilled());
        System.out.println("Perimetrul dreptunghiului este: " + dreptunghi.getPerimeter());
        System.out.println("Culoarea cercului este: " + cerc.getColor());
        System.out.println(cerc);
        System.out.println(dreptunghi);
        System.out.println(patrat);
        
    }
}
