package banciu.tudor.g30124.lab5.ex1;

abstract class Shape {
    protected String color;
    protected boolean filled;
    
    public Shape(){
        this.color="green";
        this.filled=true;
    }
    
    public Shape(String color, boolean filled){
        this.color=color;
        this.filled=filled;
    }
    
    public String getColor(){
        return this.color;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public boolean isFilled(){
        return this.filled;
    }
    
    public void setFilled(boolean filled){
        this.filled=filled;
    }
    
    abstract double getArea();
    abstract double getPerimeter();
    
    @Override
    public String toString(){
        if(this.filled=true) return "A Shape with color of "+this.color+" and filled.";
        else return "A Shape with color of "+this.color+" and not filled.";
    }
}

class Circle extends Shape{

    protected double radius;
    double pi=3.1415926535;
    
    public Circle(){
        this.radius=1.0;
    }
    
    public Circle(double radius){
        this.radius=radius;
    }
    
    public Circle(double radius, String color, boolean filled){
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }
    
    public double getRadius(){
        return this.radius;
    }
    
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    @Override
    double getArea(){
        return pi*this.radius*this.radius;
    }
    
    @Override
    double getPerimeter(){
        return 2*pi*this.radius;
    }
    
    @Override
    public String toString(){
        return "A Circle with radius= "+radius+", which is a subclass of "+super.toString();
    }
}

class Rectangle extends Shape{
    protected double width;
    protected double length;
    
    public Rectangle(){
        this.width=1.0;
        this.length=1.0;
    }
    
    public Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }
    
    public Rectangle(double width, double length, String color, boolean filled){
        this.width=width;
        this.length=length;
        this.color=color;
        this.filled=filled;
    }
    
    public double getWidth(){
        return this.width;
    }
    
    public double getLength(){
        return this.length;
    }
    
    public void setWidth(double width){
        this.width=width;
    }
    
    public void setLength(double length){
        this.length=length;
    }
    
    @Override
    double getArea(){
        return this.width*this.length;
    }
    
    @Override
    double getPerimeter(){
        return 2*(this.width+this.length);
    }
    
    @Override
    public String toString(){
        return "A Rectangle with width= "+this.width+" and length= "+this.length+", which is a class of "+super.toString();
    }
}

class Square extends Rectangle{
    public Square(){
        this.length=1.0;
        this.width=1.0;
    }
    
    public Square(double side){
        this.length=side;
        this.width=side;
    }
    
    public Square(double side,String color, boolean filled){
        this.length=side;
        this.width=side;
        this.color=color;
        this.filled=filled;
    }
    
    public double getSide(){
        return this.length;
    }
    
    public void setSide(double side){
        this.length=this.width=side;
    }
    
    @Override
    public void setLength(double length){
        this.length=length;
    }
    
    @Override
    public void setWidth(double width){
        this.width=width;
    }
    
    @Override
    public String toString(){
        return "A Square with side= "+this.length+", which is a subclass of "+super.toString();
    }
}
