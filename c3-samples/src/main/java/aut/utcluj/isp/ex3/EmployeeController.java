package aut.utcluj.isp.ex3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
;

/**
 * @author stefan
 */
public class EmployeeController {

    private ArrayList<Employee> emp = new ArrayList<>();

    public void addEmployee(final Employee employee) {
        emp.add(employee);
    }

    public Employee getEmployeeByCnp(final String cnp) {
        for(Employee employee:emp){
            if(employee.getCnp()==cnp){
                return employee;
            }
        }
        return null;
    }

    /**
     * Update employee salary by cnp
     *
     * @param cnp - unique cnp
     * @param salary - salary
     * @return updated employee
     */
    public Employee updateEmployeeSalaryByCnp(final String cnp, final Double salary) {
         for(Employee employee:emp){
             if(employee.getCnp()==cnp){
                 employee.setSalary(salary);
                 return employee;
         }
         }
         return null;
    }

    /**
     * Delete employee by cnp
     *
     * @param cnp - unique cnp
     * @return deleted employee or null if not found
     */
    public Employee deleteEmployeeByCnp(final String cnp) {       
        for(Employee e:emp){
            if(e.getCnp()==cnp){
                emp.remove(e);
                return e;
            }
        }
        return null;
          }
    

    /**
     * Return current list of employees
     *
     * @return current list of employees
     */
    public List<Employee> getEmployees() {
       return emp;
    }

    /**
     * Get number of employees
     *
     * @param o
     * @return - number of registered employees
     */
 
    public int getNumberOfEmployees() {
       return emp.size();
    }
}
