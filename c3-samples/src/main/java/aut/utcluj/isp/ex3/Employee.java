package aut.utcluj.isp.ex3;

import java.util.Objects;

/**
 * @author stefan
 */
public class Employee {
    private String firstName;
    private String lastName;
    private Double salary;
    private String cnp;

    public Employee(String firstName, String lastName, Double salary, String cnp) {
        //throw new UnsupportedOperationException("Not supported yet.");
        this.firstName=firstName;
        this.lastName=lastName;
        this.salary=salary;
        this.cnp=cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Double getSalary() {
        return salary;
    }

    public String getCnp() {
        return cnp;
    }
    
    
    
    @Override
    public String toString(){
        return "Firstname: "+firstName+" Lastname: "+" Salary: "+salary+" Cnp: "+cnp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.firstName,this.lastName,this.salary,this.cnp);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || getClass()!=o.getClass()) return false;
        Employee that=(Employee) o;
        return firstName.equals(that.firstName) && lastName.equals(that.lastName) && salary.equals(that.salary) && cnp.equals(that.cnp);
    }

    void setSalary(Double salary) {
        this.salary=salary;
    }
}
