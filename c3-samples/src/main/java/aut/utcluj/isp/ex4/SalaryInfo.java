package aut.utcluj.isp.ex4;

public class SalaryInfo {
    private Double totalRevenue;
    private Double monthlyRevenue;

    /**
     * If is not positive, an {@link NegativeAmountException} exception should be thrown
     *
     * @param monthlyRevenue
     * @throws aut.utcluj.isp.ex4.NegativeAmountException
     */
    
    public SalaryInfo(){
        totalRevenue=1d;
        monthlyRevenue=1d;
    }
    
    public SalaryInfo(Double monthlyRevenue) {
        if(monthlyRevenue>=0)
            this.monthlyRevenue = monthlyRevenue;
        else throw new NegativeAmountException();
    }

    /**
     * Add incoming salary to total revenue
     */
    public void addSalary() {
        //throw new UnsupportedOperationException("Not supported yet.");
        totalRevenue+=monthlyRevenue;
    }

    /**
     * Add certain amount of money as bonus
     * If is not positive, an {@link NegativeAmountException} exception should be thrown
     *
     * @param value - money to be added
     */
    public void addMoney(final Double value) {
        //throw new UnsupportedOperationException("Not supported yet.");
        if(value>=0){
            totalRevenue+=value;
        }
        else throw new NegativeAmountException();
        
    }

    /**
     * Pay certain amount of money as tax
     * If is not positive, an {@link NegativeAmountException} exception should be thrown
     * If not enough revenue found, an {@link NegativeBalanceException } exception should be thrown
     *
     * @param value - value to be paid
     */
    public void payTax(final Double value) {
        //throw new UnsupportedOperationException("Not supported yet.");
        if(value>=0 && totalRevenue>value)
            totalRevenue-=value;
        else if(value<0) throw new NegativeAmountException();
        else if(totalRevenue<value) throw new NegativeBalanceException();
    }

    public Double getTotalRevenue() {
        return totalRevenue;
    }

    public Double getMonthlyRevenue() {
        return monthlyRevenue;
    }
}
