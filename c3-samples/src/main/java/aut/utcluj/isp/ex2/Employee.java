package aut.utcluj.isp.ex2;

/**
 * @author stefan
 */
public class Employee extends Person{
    protected Double salary;

    public Employee(String firstName, String lastName, Double salary) {
        //throw new UnsupportedOperationException("Not supported yet.");
        this.firstName=firstName;
        this.lastName=lastName;
        this.salary=salary;
    }

    public Double getSalary() {
        return salary;
    }

    /**
     * Show employee information
     * @return employee information (Firstname: firstname Lastname: lastname Salary: salary)
     */
    public String showEmployeeInfo() {
        return "Firstname: "+ firstName + " Lastname: "+lastName+" Salary: "+salary;
    }
}
