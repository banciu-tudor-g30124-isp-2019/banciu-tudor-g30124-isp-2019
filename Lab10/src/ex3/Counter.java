package ex3;

public class Counter extends Thread {
    private Thread t;

    private Counter(Thread t) {
        this.t = t;
    }

    private static int x = 1, n1 = 0, n2 = 100;

    public void run() {
        try {
            if (t != null) {
                t.join ();
                n1 += 100;
                n2 += 100;
                x++;
            }
            for (int i = n1; i <= n2; i++) {
                System.out.println (i + ". i= " + i);
                Thread.sleep (50);
            }
            System.out.println ("Counter " + x + " ended its cycle");
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }
    }

    public static void main(String[] args) {
        Counter counter1 = new Counter (null);
        Counter counter2 = new Counter (counter1);
        Counter counter3 = new Counter (counter2);
        counter1.run ();
        try {
            Thread.sleep (2000);
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }
        counter2.run ();
        counter3.run ();
    }

}
