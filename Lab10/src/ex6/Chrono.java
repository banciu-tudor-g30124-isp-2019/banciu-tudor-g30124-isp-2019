package ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Chrono extends JFrame {
    private JLabel timeLabel;

    private JButton startButton;
    private JButton stopButton;

    private byte centiseconds = 0;
    private byte seconds = 0;
    private short minutes = 0;

    private Runnable timeTask;
    private Runnable incrementTimeTask;
    private Runnable setTimeTask;
    private DecimalFormat timeFormatter;
    private boolean timerIsRunning = false;

    private ExecutorService executor = Executors.newCachedThreadPool ();

    private Chrono() {
        JPanel panel = new JPanel ();
        panel.setLayout (new BorderLayout ());

        timeLabel = new JLabel ();
        timeLabel.setHorizontalAlignment (JLabel.CENTER);
        panel.add (timeLabel);
        JPanel buttonPanel = new JPanel ();
        buttonPanel.setLayout (new FlowLayout (FlowLayout.CENTER));
        JButton startandstopButton = new JButton ("Start/Stop");
        startandstopButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (!timerIsRunning) {
                    timerIsRunning = true;
                    executor.execute (timeTask);
                } else {
                    timerIsRunning = false;
                }
            }
        });
        buttonPanel.add (startandstopButton);

        JButton resetButton = new JButton ("Reset");
        resetButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                timerIsRunning = false;

                centiseconds = 0;
                seconds = 0;
                minutes = 0;

                timeLabel.setText (timeFormatter.format (minutes) + ":"
                        + timeFormatter.format (seconds) + "."
                        + timeFormatter.format (centiseconds));
            }
        });

        buttonPanel.add (resetButton);

        panel.add (buttonPanel, BorderLayout.SOUTH);

        timeFormatter = new DecimalFormat ("00");

        timeTask = new Runnable () {
            public void run() {
                while (timerIsRunning) {
                    executor.execute (incrementTimeTask);

                    try {
                        Thread.sleep (10);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace ();
                    }
                }
            }
        };

        incrementTimeTask = new Runnable () {
            public void run() {
                if (centiseconds > 99) {
                    centiseconds = 0;
                    seconds++;
                    if (seconds > 59) {
                        seconds = 0;
                        minutes++;
                    }
                } else {
                    centiseconds++;
                }

                executor.execute (setTimeTask);
            }
        };

        setTimeTask = new Runnable () {
            public void run() {
                timeLabel.setText (timeFormatter.format (minutes) + ":"
                        + timeFormatter.format (seconds) + "."
                        + timeFormatter.format (centiseconds));
            }
        };

        timeLabel.setText (timeFormatter.format (minutes) + ":"
                + timeFormatter.format (seconds) + "."
                + timeFormatter.format (centiseconds));

        add (panel);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo (null);
        setTitle ("Chronometer");
        pack ();
        setVisible (true);
    }

    public static void main(String[] args) {
        new Chrono ();
    }
}
