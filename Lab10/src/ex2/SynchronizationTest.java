package ex2;

public class SynchronizationTest {
    public static void main(String[] args) {
        Punct p = new Punct ();
        FireSet fs1 = new FireSet (p);
        FireGet fg1 = new FireGet (p);

        //fs1.start ();
        fs1.run ();
        //fg1.start();
        fg1.run ();
    }
}



