package ex5;

public class Test extends Thread {
    public static void main(String[] args) {
        Buffer b = new Buffer ();
        Producer pro = new Producer (b);
        Consumer c = new Consumer (b);
        Consumer c2 = new Consumer (b);
        pro.start ();
        c.run ();
        c2.run ();
    }
}
