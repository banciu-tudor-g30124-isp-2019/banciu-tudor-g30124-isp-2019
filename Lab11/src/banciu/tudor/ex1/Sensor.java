package banciu.tudor.ex1;

import java.util.*;

public class Sensor extends Observable implements Runnable {

    @Override
    public void run() {
        while (true) {
            int temp = (int) (Math.random()*100-50);
            setChanged ();
            notifyObservers (temp);
            try{
                Thread.sleep (500);
            } catch (InterruptedException e) {
                e.printStackTrace ();
            }

        }
    }

}
