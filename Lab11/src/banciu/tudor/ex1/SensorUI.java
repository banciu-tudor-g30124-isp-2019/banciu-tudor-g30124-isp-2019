package banciu.tudor.ex1;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class SensorUI extends JFrame implements Observer {


    private JTextField jTextField;

    private SensorUI() {
        setTitle ("Temperature Read");
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        init ();
        setSize (200, 250);
        Sensor sensor = new Sensor ();
        sensor.addObserver (this);
        Thread t = new Thread (sensor);
        t.start ();
    }

    private void init() {
        this.setLayout (null);
        JLabel jLabel = new JLabel ("Temp");
        jLabel.setBounds (10, 50, 80, 20);
        jTextField = new JTextField ();
        jTextField.setBounds (70, 50, 80, 20);
        jTextField.setEditable (false);
        add (jLabel);
        add (jTextField);
        setVisible (true);
    }

    public static void main(String[] args) {
        new SensorUI ();
    }

    @Override
    public void update(Observable o, Object arg) {
        jTextField.setText (arg.toString ());
    }
}