package banciu.tudor.ex2;

import java.util.ArrayList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import static java.lang.Integer.parseInt;


public class ProductStock extends JFrame {
    private ArrayList<Product> prod = new ArrayList<Product> ();
    private JButton bAddProduct;
    private JTextArea content;

    private ProductStock() {
        setTitle ("My Products");
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        init ();
        setSize (500, 650);
        setVisible (true);
    }

    private void init() {

        this.setLayout (null);
        int width = 130;
        int height = 40;
        bAddProduct = new JButton ("Add Product");
        bAddProduct.setBounds (345, 40, width, height);
        bAddProduct.addActionListener (new TratareButonAdd ());
        JButton bListProducts = new JButton ("List Products");
        bListProducts.setBounds (345, 90, width, height);
        bListProducts.addActionListener (new TratareButonList ());
        JButton bDeleteProduct = new JButton ("Delete");
        bDeleteProduct.setBounds (345, 140, width, height);
        bDeleteProduct.addActionListener (new TratareButonList ());
        JButton bChangeQuantity = new JButton ("Change Quantity");
        bChangeQuantity.setBounds (345, 190, width, height);
        bChangeQuantity.addActionListener (new TratareButonList ());

        content = new JTextArea ();
        content.setBounds (60, 40, 270, 400);
        add (bListProducts);
        add (bAddProduct);
        add (bDeleteProduct);
        add (bChangeQuantity);
        add (content);

    }


    public void changelist() {
        if (bAddProduct.isSelected ()) {
            String n;
            int q;
            double p;
            n = ProductStock.this.content.getText ();
            q = 0;
            p = 0;
            Product x;
            prod.add (new Product (n, q, p));
        }
    }

    class TratareButonList implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            add (content);
            content.setText ("");
            String filename = "C:\\Users\\Tudor Banciu\\Desktop\\Products.txt";
            String encoding = "Cp1250";
            File file1 = new File (filename);
            if (file1.exists ()) {
                try (BufferedReader br = new BufferedReader (new InputStreamReader (new FileInputStream (file1), encoding))) {
                    String line = null;
                    int k = 0;
                    ProductStock.this.content.append ("Product" + "\t  " + "Quantity" + "\t  " + "Price" + "\n");
                    while ((line = br.readLine ()) != null) {
                        if (k % 3 == 0)
                            ProductStock.this.content.append ("\n");
                        ProductStock.this.content.append (line + "\t  ");
                        k++;
                    }
                } catch (IOException e1) {
                    e1.printStackTrace ();
                }
            } else {
                ProductStock.this.content.append ("List empty");
            }

        }

    }


    class TratareButonAdd implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (String line : content.getText ().split ("\n")) {
                String[] words = line.split (" ",0);
                String n = words[0];
                int q = parseInt (words[1]);
                int p = parseInt (words[2]);
                prod.add (new Product (n, q, p));
            }
        }
    }

    public static void main(String[] args) {

        new ProductStock ();
    }
}
