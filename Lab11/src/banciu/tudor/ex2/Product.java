package banciu.tudor.ex2;

public class Product {
    private String name;
    private double quantity;
    private double price;

    Product(String name, double quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName() {
        this.name = name;
    }

    public double getQuantity() {
        return this.quantity;
    }

    public void setQuantity() {
        this.quantity = quantity;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice() {
        this.price = price;
    }

    public String toString() {
        return name + ": " + quantity + ", " + price;
    }

    public String[] transformName(String string){
        String [] words=string.split(" ",3);
        return words;
    }

    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Product)) {
            return false;
        }

        Product x = (Product) o;

        return x.name.equals (name);
    }

    public final int hashCode() {
        return (int) (name.hashCode ());
    }
}
