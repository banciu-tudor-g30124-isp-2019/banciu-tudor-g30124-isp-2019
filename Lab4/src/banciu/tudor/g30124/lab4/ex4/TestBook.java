/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab4.ex4;

import banciu.tudor.g30124.lab4.ex2.Author;
import java.util.Arrays;

/**
 *
 * @author Tudor Banciu
 */
public class TestBook {
    public static void main(String[] args){
        Author autor[]=new Author[2];
        autor[0]=new Author("Banciu Tudor", "banciut@gmail.com",'m');
        autor[1]=new Author("Balan Alexandru", "balan.alexandru@yahoo.com",'m');
        Book book=new Book("Ghid de student",autor,250,14);
        
        System.out.println(book.getName());
        System.out.println(Arrays.toString(book.getAuthor()));
        System.out.println(book.getPrice());
        System.out.println(book.getQtyInStock());
        
        book.setPrice(350);
        book.setQtyInStock(4);
        
        book.printAuthors();
    }
}
