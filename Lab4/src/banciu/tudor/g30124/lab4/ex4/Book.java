/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab4.ex4;

/**
 *
 * @author Tudor Banciu
 */
import banciu.tudor.g30124.lab4.ex2.*;

public class Book {
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock=0;
    
    public Book(String name, Author[] author, double price){
        this.name=name;
        this.author=author;
        this.price=price;
    }
    
    public Book(String name, Author[] author, double price, int qtyInStock){
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Author[] getAuthor(){
        return this.author;
    }
    
    public double getPrice(){
        return this.price;
    }
    
    public void setPrice(double price){
        this.price=price;
    }
    
    public int getQtyInStock(){
        return this.qtyInStock;
    }
    
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock=qtyInStock;
    }
    
    public void printAuthors(){
        for(Author author1:author){
            System.out.println(author1+"\n");
        }
    }
    
    @Override
    public String toString(){
        return this.name+"by "+this.author.length+" authors.";
    }
}
