/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab4.ex1;

/**
 *
 * @author Tudor Banciu
 */
public class TestCircle {
    public static void main(String[] args){
        Circle cerc= new Circle();
        Circle cerc2=new Circle(2);
        System.out.println(cerc.getRadius());
        System.out.println(cerc2.getRadius());
        System.out.println(cerc.getArea());
        System.out.println(cerc2.getArea());
        System.out.println(cerc);
        System.out.println(cerc2);
    }
}
