/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab4.ex1;

/**
 *
 * @author Tudor Banciu
 */
public class Circle {
    
    private double radius;
    private String color;
    
    public Circle(){
        this.radius=1.0;
        this.color="red";
    }
    
    public Circle(double radius){
        this.radius=radius;
    }
    
    public double getRadius(){
        return this.radius;
    }
    
    public double getArea(){
        return 3.1415926535*this.radius*this.radius;
    }
    
    public String toString(){
        return "Radius= "+ this.radius+'.'+"\nColor= "+this.color+'.';
    }
}
