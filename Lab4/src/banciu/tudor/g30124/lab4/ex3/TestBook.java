/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab4.ex3;
import banciu.tudor.g30124.lab4.ex2.*;
/**
 *
 * @author Tudor Banciu
 */
public class TestBook {
    public static void main(String[] args){
        Author autor=new Author("Banciu Tudor", "banciut@gmail.com",'m');
        Book book=new Book("Banciu Tudor - Viata",autor,250);
        Book book2=new Book("Banciu Tudor - Memorii",autor,249,100); 
        
        System.out.println(book.getName());
        System.out.println(book.getAuthor());
        System.out.println(book.getPrice());
        System.out.println(book.getQtyInStock());
        
        System.out.println(book2.getName());
        System.out.println(book2.getAuthor());
        System.out.println(book2.getPrice());
        System.out.println(book2.getQtyInStock());
        
        book.setPrice(350);
        book.setQtyInStock(4);
        
        book2.setPrice(349);
        book2.setQtyInStock(5);
        
        System.out.println(book);
        System.out.println(book2);
    }
}
