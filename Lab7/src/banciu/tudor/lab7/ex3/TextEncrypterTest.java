package banciu.tudor.lab7.ex3;

import java.io.*;
import java.nio.file.*;

public class TextEncrypterTest {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:/Users/Tudor Banciu/Desktop/text.enc");
        try {
            Files.createFile(path);
        } catch (FileAlreadyExistsException e) {
            System.err.println("already exists: " + e.getMessage());
        }

        Path path2 = Paths.get("C:/Users/Tudor Banciu/Desktop/text.dec");
        try {
            Files.createFile(path2);
        } catch (FileAlreadyExistsException f) {
            System.err.println("already exists" + f.getMessage());
        }

        Enc e = new Enc();
        e.keyEncryption(new File("C:/Users/Tudor Banciu/Desktop/data.txt"));

        Dec d = new Dec();
        d.keyDecryption("C:/Users/Tudor Banciu/Desktop/text.enc");
    }
}