package banciu.tudor.lab7.ex4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class CarTest {
    public static void main(String[] args) throws Exception {
        CarFactory m = new CarFactory();
        Car a = m.createCar("aaa", (int) (Math.random() * 1000));
        Car b = m.createCar("bbb", (int) (Math.random() * 1000));
        Path path = Paths.get("C:/Users/Tudor Banciu/Desktop/masini.txt");
        try {
            Files.createFile(path);
        } catch (FileAlreadyExistsException e) {
            System.err.println("already exists: " + e.getMessage());
        }

        m.saveCar(a, "C:/Users/Tudor Banciu/Desktop/masini.txt");
        m.saveCar(b, "C:/Users/Tudor Banciu/Desktop/masini.txt");

        m.readCar(a, "C:/Users/Tudor Banciu/Desktop/masini.txt");
        m.readCar(b, "C:/Users/Tudor Banciu/Desktop/masini.txt");

        m.showCars("C:/Users/Tudor Banciu/Desktop/masini.txt");


    }
}
