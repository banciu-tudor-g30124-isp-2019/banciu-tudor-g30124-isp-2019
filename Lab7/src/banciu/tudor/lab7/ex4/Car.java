package banciu.tudor.lab7.ex4;

import java.io.Serializable;

public class Car implements Serializable {
    public String model;
    public double price;
    transient int id;

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
        id = (int) (Math.random() * 1e+2);
    }

    @Override
    public String toString() {
        return "[model= " + model + ", price= " + price + ", id= " + id+"]";
    }
}
