/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.g30124.lab3.ex2;

/**
 *
 * @author Sala 310
 */
public class Circle {
    private double radius;
    private String color;
    
    public Circle(){
        this.radius=2.0;
        this.color="red";
    }
    
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public double getRadius(){
        return radius;
    }
    
    public double getArea(){
        return 3.1415926535*radius*radius;
    }
    
    public String getColor(){
        return color;
    }
    
    @Override
    public String toString(){
        return "Radius= " + radius + "." + "\nColor= " + color + "."+"\nArea= "+getArea()+".";
    }
}
