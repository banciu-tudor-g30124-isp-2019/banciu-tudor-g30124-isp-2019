package banciu.tudor.lab2.ex5;

import java.util.Random;

public class Test7 {
    
    static void afisare(int arr[], int n){
        for(int i=1;i<=n;i++){
            System.out.print(arr[i]);
            System.out.print(" ");
        }
        System.out.print('\n');
    }
    
    static void bule(int arr[], int n){
        int i, j, aux; 
        boolean ok; 
        for (i = 1; i < n; i++)  
        { 
            ok = false; 
            for (j = 1; j < n - i ; j++)  
            { 
                if (arr[j] > arr[j + 1])  
                { 
                    aux = arr[j]; 
                    arr[j] = arr[j + 1]; 
                    arr[j + 1] = aux; 
                    ok = true; 
                } 
            }
            if (ok == false) 
                break; 
        } 
    }
    
    public static void main(String[] args){
        Random rand= new Random();
        int[] arr= new int[11];
        for(int i=1;i<=10;i++){
            arr[i]=rand.nextInt(51);
        }
        afisare(arr,10);
        bule(arr,11);
        afisare(arr, 10);
    }
}
