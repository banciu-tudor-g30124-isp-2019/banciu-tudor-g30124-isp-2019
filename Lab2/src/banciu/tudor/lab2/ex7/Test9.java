/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.lab2.ex7;
import java.util.*;
/**
 *
 * @author Tudor Banciu
 */
public class Test9 {
    
    public static void main(String[] args){
        Random rand=new Random();
        int n= rand.nextInt(10);
        n+=1;
        Scanner keyboard=new Scanner(System.in);
        int incercari=1;
        System.out.println("Numarul cautat se afla in intervalul [1,10].");
        for(;incercari<=3;incercari++){
            int m=keyboard.nextInt();
            if(m>n){
                System.out.println("Numarul cautat este mai mic.");
            }
            else if(m<n){
                System.out.println("Numarul cautat este mai mare.");
            }
            else{
                System.out.println("Felicitari!");
                break;
            }
        }
        if(incercari>3){
                System.out.println("Ati pierdut!");
        }
            
    }
}
