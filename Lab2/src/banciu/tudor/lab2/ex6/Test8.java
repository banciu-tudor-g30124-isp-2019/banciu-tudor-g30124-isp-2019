/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banciu.tudor.lab2.ex6;
import java.util.*;

public class Test8 {
    
    static int recursiv(int n){
        if(n==0) return 1;
        return (n*recursiv(n-1));
    }
    
    static void iterativ(int n){
        int d=1;
        if(n==0) System.out.println("n!= "+"1");
        else{
            for(int i=1;i<=n;++i){
                d*=i;
            }
            System.out.println(d);
        }
    }
    
    public static void main(String[] args){
        Scanner keyboard=new Scanner(System.in);
        System.out.println("Introduceti numarul natural dorit:");
        int f=keyboard.nextInt();
        System.out.println("1.Recursiv");
        System.out.println("2.Iterativ");
        int n=keyboard.nextInt();
        switch(n){
            case 1: 
                System.out.println(recursiv(f));
                break;
            case 2:
                iterativ(f);
                break;
        }
    }
}
