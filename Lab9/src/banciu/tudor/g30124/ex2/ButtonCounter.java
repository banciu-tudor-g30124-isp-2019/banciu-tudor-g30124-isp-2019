package banciu.tudor.g30124.ex2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class ButtonCounter extends JFrame {

    public static int cc = 0;
    JButton bCounter;
    JTextField tText;

    ButtonCounter(int cc) {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);
        init();
        tText.setText(Integer.toString(cc));
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;
        tText = new JTextField();
        tText.setBounds(70, 50, width, height);
        bCounter = new JButton("Incrementare");
        bCounter.setBounds(10, 150, 200, height);
        bCounter.addActionListener(new TratareButonCounter());
        add(bCounter);
        add(tText);
    }

    public static void main(String[] args) {
        new ButtonCounter(cc);
    }

    class TratareButonCounter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            cc++;
            tText.setText(Integer.toString(cc));
        }
    }
}
